declare let coscine: {
  authorization: {
    bearer: string;
  };
  i18n: {
    projectedit: VueI18n.LocaleMessages | undefined;
  };
};
declare const _spPageContextInfo: unknown;
declare module "@itcenter-layout/bootstrap";
