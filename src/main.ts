import jQuery from "jquery";
import BootstrapVue from "bootstrap-vue";
import "@itcenter-layout/bootstrap/dist/css/rwth-theme.css";
import "@itcenter-layout/bootstrap/dist/css/material-icons.css";
import "@itcenter-layout/masterpage/dist/css/itcenter-masterdesign-masterpage.css";
import Vue from "vue";
import ProjectEditApp from "./ProjectEditApp.vue";
import VueI18n from "vue-i18n";
import { LanguageUtil } from "@coscine/app-util";

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueI18n);

jQuery(() => {
  const i18n = new VueI18n({
    locale: LanguageUtil.getLanguage(),
    messages: coscine.i18n.projectedit,
    silentFallbackWarn: true,
  });

  new Vue({
    render: (h) => h(ProjectEditApp),
    i18n,
  }).$mount("projectedit");
});
